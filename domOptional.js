// DOM - это документ в памяти компьютера, создаваемый браузером на основе полученого им кода, позволяющий
// скриптам получать доступ к самим документам html и xhtml, аля програмный интерфейс 
const cities = ["Kharkiv", "Kiev", ["Uzhorod", ["Irpin", "Borispol"], "Lviv"], "Dnieper"];
function getData(array, parent) {
    function rec(array) {
        let list = array.map( element => {
            if (Array.isArray(element)) { 
                return rec(element)
            }
            return `<li>${element}</li>`
        })
        list = list.join('')
        list = `<ol>${list}</ol>`
        return list
    }
    console.log(rec(array))
    parent.insertAdjacentHTML('afterbegin', rec(array))
}
getData(cities, document.body)